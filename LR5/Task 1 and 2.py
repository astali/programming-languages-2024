import math

import pygame
from pygame.draw import *
from random import randint

pygame.init()

FPS = 2
screen = pygame.display.set_mode((1200, 900))

RED = (255, 0, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
GREEN = (0, 255, 0)
MAGENTA = (255, 0, 255)
CYAN = (0, 255, 255)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

COLORS = [RED, BLUE, YELLOW, GREEN, MAGENTA, CYAN]


def new_ball():
    '''рисует новый шарик '''
    global x, y, r
    x = randint(100, 1100)
    y = randint(100, 900)
    r = randint(10, 100)
    color = COLORS[randint(0, 5)]
    circle(screen, color, (x, y), r)


def click(event, a):
    mouse_x, mouse_y = pygame.mouse.get_pos()
    if (math.sqrt(pow(mouse_x, 2) + pow(mouse_y, 2)) - math.sqrt (pow(x, 2) + pow(y, 2)))<=r:
        a += 1
    return a


screen.fill(WHITE)

pygame.display.update()
clock = pygame.time.Clock()
finished = False
score = 0

while not finished:
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            finished = True
        elif event.type == pygame.MOUSEBUTTONDOWN:
            score = click(pygame.MOUSEBUTTONDOWN, score)
            print(score)
    new_ball()
    pygame.display.update()
    screen.fill(WHITE)

pygame.quit()
