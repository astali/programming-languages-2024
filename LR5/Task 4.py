import pygame
from pygame.draw import *
from random import randint
import math

pygame.init()

FPS = 60
width = 1200
height = 800
screen = pygame.display.set_mode((width, height))

RED = (255, 0, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
GREEN = (0, 255, 0)
MAGENTA = (255, 0, 255)
CYAN = (0, 255, 255)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
COLORS = [RED, BLUE, YELLOW, GREEN, MAGENTA, CYAN]
screen.fill(WHITE)


def new_ball():
    '''рисует новый шарик '''
    global x, y, r
    x = randint(100, 1100)
    y = randint(100, 700)
    # r = randint(10, 100)

    r = 50
    color = COLORS[randint(0, 5)]
    return circle(screen, color, (x, y), r)


def click(event, a, unit):  ## проверяет на попадане по шарику
    for ball in unit:
        mouse_x, mouse_y = pygame.mouse.get_pos()
        ball_coord = ball.center
        print(ball_coord)
        print(mouse_x, mouse_y)
        if ((math.sqrt(pow(mouse_x, 2) + pow(mouse_y, 2))) - math.sqrt(pow(ball_coord[0], 2) + pow(ball_coord[1], 2)))<=r:
            a += 1
            screen.fill(WHITE)
    return a


def ball_move(ball_obj, speed):  ##вычисляет следующую точку шарика и рисует в ней новый
    ball_obj = ball_obj.move(speed)

    if ball_obj.left <= 0 or ball_obj.right >= width: ## проверка на выход за границы
        speed[0] = -speed[0]
    if ball_obj.top <= 0 or ball_obj.bottom >= height:
        speed[1] = -speed[1]

    pygame.draw.circle(surface=screen, color=RED, center=ball_obj.center, radius=50)
    return ball_obj


pygame.display.update()
clock = pygame.time.Clock()
finished = False
score = 0

speed = [[4, 4], [-4, -4], [-4, 4], [4, -4]]

unit = [new_ball() for i in range(4)]

while not finished:
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            finished = True
        elif event.type == pygame.MOUSEBUTTONDOWN:
            score = click(pygame.MOUSEBUTTONDOWN, score, unit)
            print(score)
            unit = [new_ball() for i in range(4)]

    screen.fill(WHITE)
    for i in range(4):
        unit[i] = ball_move(unit[i], speed[i])
    # update screen
    pygame.display.flip()
    pygame.display.update()

pygame.quit()
