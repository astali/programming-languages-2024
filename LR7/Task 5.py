import random
import tkinter
import time
from random import randrange as rnd, choice
from tkinter import *
import math
import time
import types
root = Tk()
fr = Frame(root)
root.geometry('800x600')
canv = Canvas(root, bg='white')
canv.pack(fill=BOTH, expand=1)

class ball():
    def __init__(self, x, y):
        global flag
        self.x = x
        self.y = y
        self.r = 10
        self.vx = 0
        self.vy = 0
        self.color = choice(['blue', 'green', 'red', 'brown'])
        #self.id = canv.create_oval(self.x - self.r, self.y - self.r, self.x + self.r, self.y + self.r, fill=self.color)
        if flag %2==0 :
            self.id = canv.create_oval(self.x - self.r, self.y - self.r, self.x + self.r, self.y + self.r,
                                       fill=self.color)
        else:
            self.id = canv.create_rectangle(self.x - self.r, self.y - self.r, self.x + self.r, self.y + self.r,
                                            fill=self.color)
        self.live = 30

    def set_coords(self):
        canv.coords(self.id, self.x - self.r, self.y - self.r, self.x + self.r, self.y + self.r)
        '''
    def id1(self,event):
        #if canv.event_info()==
        self.id = canv.create_oval(self.x - self.r, self.y - self.r, self.x + self.r, self.y + self.r, fill=self.color)
        self.id = canv.create_rectangle(self.x - self.r, self.y - self.r, self.x + self.r, self.y + self.r,
                                            fill=self.color)'''

    def move(self):
        if self.y <= 500:
            self.vy -= 1.2
            self.y -= self.vy
            self.x += self.vx
            self.vx *= 0.99
            self.set_coords()
        else:
            if self.vx ** 2 + self.vy ** 2 > 10:
                self.vy = -self.vy / 2
                self.vx = self.vx / 2
                self.y = 499
            if self.live < 0:
                balls.pop(balls.index(self))
                canv.delete(self.id)
            else:
                self.live -= 1
        if self.x > 780:
            self.vx = -self.vx / 2
            self.x = 779
    def moveX(self):
        self.x -= self.vx
        self.set_coords()
        if self.x > 780:
            self.vx = -self.vx / 2
            self.x = 779


    def hittest(self, ob):
        if abs(ob.x - self.x) <= (self.r + ob.r) and abs(ob.y - self.y) <= (self.r + ob.r):
            return True
        else:
            return False


"""
\xd0\x9a\xd0\xbb\xd0\xb0\xd1\x81\xd1\x81 gun \xd0\xbe\xd0\xbf\xd0\xb8\xd1\x81\xd1\x8b\xd0\xb2\xd0\xb0\xd0\xb5\xd1\x82 \xd0\xbf\xd1\x83\xd1\x88\xd0\xba\xd1\x83. 
"""


class gun():
    def __init__(self):
        self.f2_power = 10
        self.f2_on = 0
        self.an = 1
        self.x = 20
        self.y = random.randint(100,500)
        self.id = canv.create_line(self.x, self.y, self.x+30, self.y-30, width=7)
        self.vy=5

    def fire2_start(self, event):
        self.f2_on = 1

    def fire2_end(self, event):
        global balls, bullet
        bullet += 1
        new_ball = ball(self.x,self.y)
        new_ball.r += 5
        self.an = math.atan((event.y - new_ball.y) / (event.x - new_ball.x))
        new_ball.vx = self.f2_power * math.cos(self.an)
        new_ball.vy = -self.f2_power * math.sin(self.an)
        balls += [new_ball]
        self.f2_on = 0
        self.f2_power = 10

    def targetting(self, event=0):
        if event:
            self.an = math.atan((event.y - 450) / (event.x - 20))
        if self.f2_on:
            canv.itemconfig(self.id, fill='orange')
        else:
            canv.itemconfig(self.id, fill='black')
        canv.coords(self.id, self.x, self.y, self.x + max(self.f2_power, self.x) * math.cos(self.an),
                    self.y + max(self.f2_power, self.x) * math.sin(self.an))

    def power_up(self):
        if self.f2_on:
            if self.f2_power < 100:
                self.f2_power += 1
            canv.itemconfig(self.id, fill='orange')
        else:
            canv.itemconfig(self.id, fill='black')

    def gun_moveD(self,event):
        self.y += self.vy
    def gun_moveU(self,event):
        self.y -= self.vy
"""
\xd0\x9a\xd0\xbb\xd0\xb0\xd1\x81\xd1\x81 target \xd0\xbe\xd0\xbf\xd0\xb8\xd1\x81\xd1\x8b\xd0\xb2\xd0\xb0\xd0\xb5\xd1\x82 \xd1\x86\xd0\xb5\xd0\xbb\xd1\x8c. 
"""

class Enemy_gun(gun):
    def __init__(self):
        self.x=600
        self.f2_power = 10
        self.f2_on = 0
        self.an = 1

        self.y = random.randint(100,500)

        self.id = canv.create_line(self.x, self.y, self.x-30, self.y, width=7)
        canv.itemconfig(self.id, fill='purple')
        self.vy=5

    def scope(self):
         canv.coords(self.id,self.x, self.y, self.x-30, self.y)
    def move(self):
        self.y+=self.vy
        self.scope()
        if self.y>=600 or self.y<=0:
            self.vy=-self.vy
    def fire(self,event):
        global balls, bullet

        bullet += 1
        new_ball = ball(self.x-30,self.y)
        new_ball.r += 5
        new_ball.vx = -self.f2_power
        new_ball.vy = -self.f2_power
        balls += [new_ball]
        self.f2_on = 0
        self.f2_power = 10

    def fire2_start(self, event):
        self.f2_on = 1
    def power_up(self):
        if self.f2_on:
            if self.f2_power < 100:
                self.f2_power += 1
            canv.itemconfig(self.id, fill='green')
        else:
            canv.itemconfig(self.id, fill='purple')
    def targetting(self, event=0):
        if self.f2_on:
            canv.itemconfig(self.id, fill='green')
        else:
            canv.itemconfig(self.id, fill='purple')
        canv.coords(self.id, self.x, self.y, self.x + max(self.f2_power, self.x),
                    self.y + max(self.f2_power, self.x))


class target():
    def __init__(self):
        self.points = 0
        self.id = canv.create_oval(0, 0, 0, 0)
        self.id_points = canv.create_text(30, 30, text=self.points, font='28')
        self.new_target()
        self.live = 1
        self.v=5
    def new_target(self):
        x = self.x = rnd(600, 780)
        y = self.y = rnd(300, 550)
        r = self.r = rnd(2, 50)
        self.vx = rnd(5,10)
        self.vy = -rnd(5,10)

        color = self.color = 'red'
        canv.coords(self.id, x - r, y - r, x + r, y + r)
        canv.itemconfig(self.id, fill=color)

    def hit(self, points=1):
        canv.coords(self.id, -10, -10, -10, -10)
        self.points += points
        canv.itemconfig(self.id_points, text=self.points)
    def move(self):
        self.y += self.vy
        self.x += self.vx
        self.set_coords()
        if self.y>=600 or self.y<=0:
            self.vy=-self.vy
        if self.x>=780 or self.x<=640:
            self.vx=-self.vx

    def set_coords(self):
        canv.coords(self.id, self.x - self.r, self.y - self.r, self.x + self.r, self.y + self.r)

class bomb():
    def __init__(self):
        self.points = 0
        self.id = canv.create_oval(0, 0, 0, 0)
        self.new_target()
        self.live = 1

    def new_target(self):
        x = self.x = rnd(600, 780)
        y = self.y = rnd(300, 550)
        r = self.r = 10
        self.vx = rnd(5, 10)
        self.vy = -rnd(5, 10)

        color = self.color = 'black'
        canv.coords(self.id, x - r, y - r, x + r, y + r)
        canv.itemconfig(self.id, fill=color)
    def hit(self):
        canv.coords(self.id, -10, -10, -10, -10)

    def move(self):
        #self.y += self.vy
        self.x += self.vx
        self.set_coords()

        if self.x >= 780 or self.x <= 0:
            self.vx = -self.vx

    def set_coords(self):
        canv.coords(self.id, self.x - self.r, self.y - self.r, self.x + self.r, self.y + self.r)


def click ():
    global flag
    flag+=1

btn = tkinter.Button(text='type', command=click, padx=10, pady=10)
btn.pack()

t1 = target()
t2 = target()
bo1 = bomb()
screen1 = canv.create_text(400, 300, text='', font='28')
g1 = gun()
bullet = 0
balls = []
eg =Enemy_gun()
eg_balls = []
eg_bullet = 0
flag=0


def new_game(event=''):
    global gun, t1,t2, screen1, balls, bullet,bombs,bo1,eg_balls,eg_bullet
    t1.new_target()
    t2.new_target()
    bo1.new_target()
    bullet = 0
    eg_bullet = 0
    balls = []
    eg_balls = []
    prev_time = 0
    cu_time = 0
    canv.bind('<Button-1>', g1.fire2_start)
    canv.bind('<ButtonRelease-1>', g1.fire2_end)
    canv.bind('<Motion>', g1.targetting)
    canv.bind('<w>',g1.gun_moveU)
    canv.bind('<s>',g1.gun_moveD)
    canv.bind('<Return>',g1.gun_moveD)
    canv.bind('<Button-3>', eg.fire2_start)
    canv.bind('<ButtonRelease-3>',eg.fire)


    z = 0.03
    t1.live = 1
    t2.live = 1
    bo1.live = 1
    while (t1.live or t2.live) and bo1.live or balls:

        eg.move()

        if t1.live:
            t1.move()
        if t2.live:
            t2.move()
        if bo1.live:
            bo1.move()
        aft=time.time()
        for b in balls:
            b.move()

            if b.hittest(t1) and t1.live:
                t1.live = 0
                t1.hit()
                #canv.bind('<Button-1>', '')
                    #canv.bind('<ButtonRelease-1>', '')
            if b.hittest(bo1) and bo1.live:
                bo1.live = 0
                bo1.hit()

            if b.hittest(t2) and t2.live:
                t2.live = 0
                t2.hit()

                    #canv.bind('<Button-1>', '')
                    #canv.bind('<ButtonRelease-1>', '')

            if (t2.live==0 and t1.live==0) or bo1.live==0:
                canv.bind('<Button-1>', '')
                canv.bind('<ButtonRelease-1>', '')
        if  not (t1.live or t2.live):
            canv.itemconfig(screen1, text='Вы уничтожили цели за ' + str(bullet) + ' выстрелов')
        if not bo1.live:
            canv.itemconfig(screen1, text='Вы взорвались ')
        canv.update()
        time.sleep(0.03)
        g1.targetting()
        eg.power_up()
        g1.power_up()
    canv.itemconfig(screen1, text='')
    canv.delete(gun)
    root.after(750, new_game)


new_game()
canv.mainloop()
