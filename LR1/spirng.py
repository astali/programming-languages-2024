import turtle
import math

turtle.shape('turtle')
turtle.speed("fastest")


def halfcirle(n):
    for k in range(45):
        turtle.forward(n)
        turtle.right(4)


turtle.left(90)
for i in range(5):
    halfcirle(4)
    halfcirle(1)
