import turtle
import math

turtle.shape('turtle')


def circle(a):
    if (a % 2 == 0):
        for j in range(90):
            turtle.forward(4)
            turtle.right(4)
    elif (a % 2 != 0):
        for j in range(90):
            turtle.forward(4)
            turtle.left(4)


for i in range(3):
    circle(1)
    circle(2)
    turtle.right(60)
