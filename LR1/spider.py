import turtle

turtle.shape('turtle')
n=12
for i in range(1,n+1):
    turtle.forward(100)
    turtle.stamp()
    turtle.right(180)
    turtle.forward(100)
    turtle.right(180)
    turtle.left(30)
