import turtle
import math

turtle.shape('turtle')
turtle.speed("fastest")


def circle(a, n):
    if (a % 2 == 0):
        for i in range(90):
            turtle.forward(n)
            turtle.right(4)
    elif (a % 2 != 0):
        for i in range(90):
            turtle.forward(n)
            turtle.left(4)


turtle.right(90)
for j in range(1, 11):
    circle(1, j)
    circle(2, j)
