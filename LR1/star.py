import turtle
import math

turtle.shape('turtle')


# turtle.speed("fastest")
def star(n):
    turtle.penup()
    turtle.left(18)
    turtle.forward(90)
    turtle.pendown()
    for i in range(n):
        turtle.left(180 - (180/n))
        turtle.forward(180)


turtle.left(90)
star(11)

# turtle.exitonclick()
