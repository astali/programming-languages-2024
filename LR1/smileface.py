import turtle
import math

turtle.shape('turtle')
turtle.speed("fastest")


def circle(a, n):
    if (a % 2 == 0):
        for i in range(90):
            turtle.forward(n)
            turtle.right(4)
    elif (a % 2 != 0):
        for i in range(90):
            turtle.forward(n)
            turtle.left(4)


def halfcirle(n):
    for k in range(45):
        turtle.forward(n)
        turtle.right(4)


turtle.pensize(5)

turtle.begin_fill()
circle(2, 6)
turtle.color('yellow')
turtle.end_fill()
turtle.color('black')

turtle.penup()
turtle.home()
turtle.goto(-30, -40)

turtle.pendown()
turtle.begin_fill()
circle(2, 1)
turtle.color('blue')
turtle.end_fill()
turtle.color('black')

turtle.penup()
turtle.goto(30, -40)

turtle.pendown()
turtle.begin_fill()
circle(2, 1)
turtle.color('blue')
turtle.end_fill()
turtle.color('black')

turtle.penup()

turtle.goto(0, -60)
turtle.pendown()
turtle.goto(0, -100)

turtle.penup()

turtle.goto(30, -130)

turtle.pendown()
turtle.right(90)
turtle.color('red')

halfcirle(2)
