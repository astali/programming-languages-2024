import random
import sys

sys.setrecursionlimit(3000)


def apple_field(n, m):
    field = [0] * n
    for a in range(n):
        field[a] = [0] * m

    for i in range(n):
        for j in range(m):
            field[i][j] = random.randint(0, 1)
    field[random.randint(0, n - 1)][random.randint(0, m - 1)] = 2
    return field


def find2(a, n, m):
    for i in range(n):
        for j in range(m):
            if a[i][j] == 2:
                return i, j


def count1(a, n, m):
    c = 0
    for i in range(n):
        for j in range(m):
            if a[i][j] == 1:
                c += 1
    return c


def bad_consume(a, x, y):
    if a[x + 1][y] == 1:
        a[x + 1][y] = 2
    if a[x - 1][y] == 1:
        a[x - 1][y] = 2
    if a[x][y + 1] == 1:
        a[x][y + 1] = 2
    if a[x][y - 1] == 1:
        a[x][y - 1] = 2

def angle(a,n,m):
    if a[0][0] == 2:
        x = 0
        y = 0
        if a[x + 1][y] == 1:
            a[x + 1][y] = 2

        if a[x][y + 1] == 1:
            a[x][y + 1] = 2


    if a[n-1][m - 1] == 2:
        x = n - 1
        y = m - 1
        if a[x - 1][y] == 1:
            a[x - 1][y] = 2
        if a[x][y - 1] == 1:
            a[x][y - 1] = 2


    if a[n - 1][0] == 2:
        x = n - 1
        y = 0

        if a[x - 1][y] == 1:
            a[x - 1][y] = 2
        if a[x][y + 1] == 1:
            a[x][y + 1] = 2


    if a[0][m - 1] == 2:
        x = 0
        y = m - 1
        if a[x + 1][y] == 1:
            a[x + 1][y] = 2
        if a[x][y - 1] == 1:
            a[x][y - 1] = 2
def sides(a,n,m):
    for i in range(1,n-1):
        if a[i][0] == 2:
            if a[i + 1][0] == 1:
                a[i + 1][0] = 2
            if a[i - 1][0] == 1:
                a[i - 1][0] = 2
            if a[i][1] == 1:
                a[i][1] = 2
        if a[i][m-1]==2:
            if a[i + 1][m-1] == 1:
                a[i + 1][m-1] = 2
            if a[i - 1][m-1] == 1:
                a[i - 1][m-1] = 2
            if a[i][m-2] == 1:
                a[i][m-2] = 2
    for j in range(1,m-1):
        if a[0][j] == 2:
            if a[0+1][j] == 1:
                a[0+1][j] = 2
            if a[0][j + 1] == 1:
                a[0][j + 1] = 2
            if a[0][j - 1] == 1:
                a[0][j - 1] = 2

        if a[n-1][j] == 2 :
            if a[n-1 - 1][j] == 1:
                a[n-1 - 1][j] = 2
            if a[n-1][j + 1] == 1:
                a[n-1][j + 1] = 2
            if a[n-1][j - 1] == 1:
                a[n-1][j - 1] = 2
def main_part(a,n,m):
    for k in range(1, n - 1):
        for g in range(1, m - 1):
            if a[k][g] == 2:
                if a[k + 1][g] == 1:
                    a[k + 1][g] = 2
                if a[k - 1][g] == 1:
                    a[k - 1][g] = 2
                if a[k][g + 1] == 1:
                    a[k][g + 1] = 2
                if a[k][g - 1] == 1:
                    a[k][g - 1] = 2

def bad(a, n, m, t):
    a_past = a
    """
    for i in range(0, n):
        for j in range(0, m):
            if a[i][j] == 2 and i == 0 and (j < m - 1 and j > 0):
                x = 0
                y = j
                if a[x + 1][y] == 1:
                    a[x + 1][y] = 2
                if a[x][y + 1] == 1:
                    a[x][y + 1] = 2
                if a[x][y - 1] == 1:
                    a[x][y - 1] = 2

            if a[i][j] == 2 and i == n - 1 and (j < m - 1 and j > 0):
                x = i
                y = j
                if a[x - 1][y] == 1:
                    a[x - 1][y] = 2
                if a[x][y + 1] == 1:
                    a[x][y + 1] = 2
                if a[x][y - 1] == 1:
                    a[x][y - 1] = 2

            if a[i][j] == 2 and j == 0 and (i < n - 1 and i > 0):
                x = i
                y = 0
                if a[x + 1][y] == 1:
                    a[x + 1][y] = 2
                if a[x - 1][y] == 1:
                    a[x - 1][y] = 2
                if a[x][y + 1] == 1:
                    a[x][y + 1] = 2

            if a[i][j] == 2 and j == m - 1 and (i < n - 1 and i > 0):
                x = i
                y = j
                if a[x + 1][y] == 1:
                    a[x + 1][y] = 2
                if a[x - 1][y] == 1:
                    a[x - 1][y] = 2
                if a[x][y - 1] == 1:
                    a[x][y - 1] = 2
        """
    while True:
        angle(a,n,m)
        sides(a,n,m)
        main_part(a,n,m)
        t+=1
        angle(a,n,m)
        sides(a,n,m)
        main_part(a,n,m)
        t+=1
        angle(a,n,m)
        sides(a,n,m)
        main_part(a,n,m)
        t+1
        if (count1(a, n, m) == 0):
            return t
        if (count1(a, n, m) > 0):
            if (a != a_past):
                t += 1
                return bad(a, n, m, t)
            elif (a==a_past ):
                return -1


n = 5
m = 5
apples = apple_field(n, m)
#apples=[[2,1,1,1],[0,1,1,1],[0,0,0,1],[1,1,1,1]]
for i in range(n):
    print(apples[i])
result = bad(apples, n, m, 0)
#result = bad(apples, n, m, 0)
print("")
print(result)
print("")
for i in range(n):
    print(apples[i])gt
