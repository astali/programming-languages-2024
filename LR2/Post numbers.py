import turtle
from turtle import *
import math




speed(3)
numbers = [(10, -10, 90, 10, -90, 10, -90, 20, -90, 10, -90, 10, -90),
           (10, -10, 45, 10 * math.sqrt(2), -135, 20, 90),
           (10, 0, 0, 10, -90, 10, -45, 10 * math.sqrt(2), 135, 10),
           (10, 0, 0, 10, -135, 10 * math.sqrt(2), 135, 10, -135, 10 * math.sqrt(2), 135),
           (10, 0, -90, 10, 90, 10, 90, 10, 180, 20, 90),
           (10, 0, 0, 10, 0, -10, -90, 10, 90, 10, -90, 10),
           (10, -10, 0, 10, -90, 10, -90, 10, -90, 10, -45, 10 * math.sqrt(2), -45),
           (10, 0, 0, 10, -135, 10 * math.sqrt(2), 45, 10, 90),
           (10, 0, -90, 20, 90, 10, 90, 20, 90, 10, 90, 10, 90, 10),
           (10, 0, -90, 10, 90, 10, 90, 10, 90,10,90,10,90,10,-135,10*math.sqrt(2),135)]

# test = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
test = (9, 1, 2)
index = (1, 4, 1, 7, 3, 0)
a = 2
for i in index:
    penup()
    goto(numbers[i][0] * a, numbers[i][1])
    pendown()
    a += 2
    for j in range(2, len(numbers[i])):
        if j % 2 == 0:
            left(numbers[i][j])
        else:
            forward(numbers[i][j])
turtle.penup()
turtle.forward(1000000000000)