from turtle import *
import math

f = open('strange numbers', 'w')

numbers_wrong = [(10, -10, 45, 10 * math.sqrt(2), -135, 20, 90),
                 (10, 0, -90, 10, 90, 10, 90, 10, 180, 20, 90),
                 (10, -10, 45, 10 * math.sqrt(2), -135, 20, 90),
                 (10, 0, 0, 10, -135, 10 * math.sqrt(2), 45, 10, 90),
                 (10, 0, 0, 10, -135, 10 * math.sqrt(2), 135, 10, -135, 10 * math.sqrt(2), 135),
                 (10, -10, 90, 10, -90, 10, -90, 20, -90, 10, -90, 10, -90)

                 ]

print(10, -10, 45, 10 * 1.41, -135, 20, 90,'\n',
      10, 0, -90, 10, 90, 10, 90, 10, 180, 20, 90,'\n',
      10, -10, 45, 10 * 1.41, -135, 20, 90,'\n',
      10, 0, 0, 10, -135, 10 * 1.41, 45, 10, 90,'\n',
      10, 0, 0, 10, -135, 10 * 1.41, 135, 10, -135, 10 * 1.41, 135,'\n',
      10, -10, 90, 10, -90, 10, -90, 20, -90, 10, -90, 10, -90, file=f)
f.close()

f = open('strange numbers', 'r')


index = (1, 4, 1, 7, 3, 0)
a = 2
for i in index:
    numbers_true = f.readline()
    numbers = list(map(float, numbers_true.split()))
    penup()
    goto(numbers[0] * a, numbers[1])
    pendown()
    a += 2
    for j in range(2, len(numbers)):
        if j % 2 == 0:
            left(numbers[j])
        else:
            forward(numbers[j])
