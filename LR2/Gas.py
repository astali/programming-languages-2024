import turtle
from random import randint
from turtle import Turtle


def Rectangle(WIDTH, HEIGHT):
    t.pendown()
    for i in range(2):
        t.forward(WIDTH)
        t.left(90)
        t.forward(HEIGHT)
        t.left(90)
    t.penup()


t = Turtle("circle", visible=False)

number_of_turtles = 100
steps_of_time_number = 100

WIDTH = 400
HEIGHT = 400
t.speed('fastest')
t.pensize(5)
t.penup()
t.goto(-WIDTH / 2, -HEIGHT / 2)
Rectangle(WIDTH, HEIGHT)

t.home()

direct = randint(1, 360)
t.setheading(direct)

CURSOR_SIZE = 20

pool = [turtle.Turtle(shape="circle") for i in range(50)]

for unit in pool:
    unit.penup()
    unit.speed(50)
    unit.goto(randint(-100, 100), randint(-100, 100))
    unit.left(randint(1, 360))


while True:
    for unit in pool:
        unit.speed(50)
        unit.forward(20)
        ty = unit.ycor()
        # breaking out top or bottom
        if not CURSOR_SIZE / 2 - HEIGHT / 2 <= ty <= HEIGHT / 2 - CURSOR_SIZE / 2:

            angleCurr = unit.heading()
            if 0 < angleCurr < 180:
                unit.setheading(0 - angleCurr)
            else:
                unit.setheading(360 - angleCurr)
            unit.forward(20)
        tx = unit.xcor()
        # breaking out left or right
        if not CURSOR_SIZE / 2 - WIDTH / 2 <= tx <= WIDTH / 2 - CURSOR_SIZE / 2:

            angleCurr = unit.heading()
            if 0 < angleCurr < 180:
                unit.setheading(180 - angleCurr)
            else:
                unit.setheading(540 - angleCurr)
            unit.forward(20)
