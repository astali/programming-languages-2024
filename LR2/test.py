from turtle import Turtle
from random import randint
COLORS = ['red', 'blue', 'green', 'yellow', 'orange', "purple", "pink", "turquoise"]
WIDTH, HEIGHT = 400, 200
CURSOR_SIZE = 20
def Rectangle():
    t.pendown()
    for i in range(2):
        t.forward(WIDTH)
        t.left(90)
        t.forward(HEIGHT)
        t.left(90)
    t.penup()
def tDirection(direct):
    t.setheading(direct)
t = Turtle("circle", visible=False)
t.speed('fastest')
t.pensize(5)
t.penup()
t.goto(-WIDTH/2, -HEIGHT/2)
Rectangle()
index = 0
t.color(COLORS[index % len(COLORS)])
t.home()
t.showturtle()
direct = randint(1, 360)
tDirection(direct)
while True:
    t.forward(2)
    ty = t.ycor()
    # breaking out top or bottom
    if not CURSOR_SIZE/2 - HEIGHT/2 <= ty <= HEIGHT/2 - CURSOR_SIZE/2:
        index += 1
        t.color(COLORS[index % len(COLORS)])
        angleCurr = t.heading()
        if 0 < angleCurr < 180:
            tDirection(0 - angleCurr)
        else:
            tDirection(360 - angleCurr)
        t.forward(2)
    tx = t.xcor()
    # breaking out left or right
    if not CURSOR_SIZE/2 - WIDTH/2 <= tx <= WIDTH/2 - CURSOR_SIZE/2:
        index += 1
        #t.color(COLORS[index % len(COLORS)])
        angleCurr = t.heading()
        if 0 < angleCurr < 180:
            tDirection(180 - angleCurr)
        else:
            tDirection(540 - angleCurr)
        t.forward(2)