from turtle import *
from math import *

ay = -10
Vx = 10
X = 0
Y = 0
Vy = 40
dt = 0
for i in range(10):
    while True:
        Vy += ay * dt
        X += Vx * dt
        Y += Vy * dt + (ay * (dt ** 2)) / 2
        goto(X, Y)
        dt += 10 ** (-3)
        if Y < 0: break
    Vy = abs(Vy / 1.3)

penup()
forward(1000000000000)